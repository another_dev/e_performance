// ----------------------------------------------------------------------------------------------
// Copyright 2017 Mårten Rånge
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ----------------------------------------------------------------------------------------------

// g++ --std=c++14 -pipe -Wall -O3 -ffast-math -fno-finite-math-only -march=native -mfpmath=sse -msse3 -fopenmp mandelbrot.cc

#include <array>
#include <cstddef>
#include <cstdio>
#include <chrono>
#include <vector>
#include <tuple>

namespace
{
  constexpr auto    min_x    = -1.5 ;
  constexpr auto    min_y    = -1.0 ;
  constexpr auto    max_x    =  0.5 ;
  constexpr auto    max_y    =  1.0 ;
  constexpr auto    max_iter =  50U ;

  template<typename T>
  auto time_it (T a)
  {
    auto before = std::chrono::high_resolution_clock::now ();
    a ();
    auto after  = std::chrono::high_resolution_clock::now ();
    auto diff   = std::chrono::duration_cast<std::chrono::milliseconds> (after - before).count ();
    return diff;
  }

  auto mandelbrot (double cx, double cy)
  {
    auto x    = cx      ;
    auto y    = cy      ;
    auto iter = max_iter;
    for (; iter > 0; --iter)
    {
      auto x2 = x*x;
      auto y2 = y*y;
      if (x2 + y2 > 4)
      {
        return iter;
      }
      y = (x + x) * y + cy ;
      x = x2 - y2 + cx     ;
    }

    return iter;
  }

  void compute_set (std::size_t const dim   ,
                    std::size_t const width ,
                    uint8_t**         set   )
  {
    auto scalex = (max_x - min_x) / dim;
    auto scaley = (max_y - min_y) / dim;

    #pragma omp parallel for collapse (2)
    for (auto y = 0U; y < dim; ++y)
    {
      for (auto w = 0U; w < width; ++w)
      {
        auto bits = 0U;
        for (auto bit = 0U; bit < 8U; ++bit)
        {
          auto x = w*8 + bit;

          auto i = mandelbrot (scalex*x + min_x, scaley*y + min_y);

          if (i == 0)
          {
            bits |= 1 << (7U - bit);
          }
        }
        set[y][w] = bits;
      }
    }
  }

}

int main (int argc, char const * argv[])
{
  auto dim  = [argc, argv] ()
  {
    auto dim = argc > 1 ? atoi (argv[1]) : 0;
    return dim > 0 ? dim : 200;
  } ();

  if (dim % 8 != 0)
  {
    std::printf ("Dimension must be modulo 8\n");
    return 999;
  }

  std::printf ("Generating mandelbrot set %dx%d(%d)\n", dim, dim, max_iter);

  auto width = (dim - 1) / 8 + 1;

  // Initialize two-dimensional array
  uint8_t** set = new uint8_t*[dim];
  for (auto i = 0; i < dim; ++i)
  {
    set[i] = new uint8_t[width];
  }

  auto ms  = time_it ([dim, width, set] { return compute_set(dim, width, set); });

  std::printf ("  it took %lld ms\n", ms);

  auto file = std::fopen ("mandelbrot_reference.pbm", "wb");

  // Flatten the two-dimensional array and free the memory
  std::vector<uint8_t> flat;
  for (auto i = 0; i < dim; ++i)
  {
    for (auto j = 0; j < width; ++j)
    {
      flat.push_back (set[i][j]);
    }

    delete[] set[i];
  }

  delete[] set;

  std::fprintf (file, "P4\n%d %d\n", dim, dim);
  std::fwrite (&flat.front (), 1, flat.size (), file);

  std::fclose (file);

  return 0;
}


